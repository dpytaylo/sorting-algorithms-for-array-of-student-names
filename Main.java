import java.util.Arrays;

public class Main {
    enum Ordering {
        Less,
        Equal,
        Greater,
    }

    public static Ordering compare(String a, String b) {
        for (int i = 0; i < Integer.min(a.length(), b.length()); i++) {
            if (a.charAt(i) > b.charAt(i)) {
                return Ordering.Greater;
            } else if (a.charAt(i) < b.charAt(i)) {
                return Ordering.Less;
            }
        }

        var deltaLenght = a.length() - b.length();

        if (deltaLenght == 0) {
            return Ordering.Equal;
        } else if (deltaLenght > 0) {
            return Ordering.Greater;
        } else {
            return Ordering.Less;
        }
    }

    public static void directSelectionSort(String[] names, String[] surnames) {
        for (int i = 0; i < names.length; i++) {
            String min_name = null;
            String min_surname = null;
            int index = i;

            for (int j = i + 1; j < names.length; j++) {
                if (compare(names[j], names[i]) == Ordering.Less
                    || compare(names[j], names[i]) == Ordering.Equal
                    && compare(surnames[j], surnames[i]) == Ordering.Less
                ) {
                    if (min_name != null) {
                        if (compare(names[j], min_name) == Ordering.Greater
                            || compare(names[j], min_name) == Ordering.Equal
                            && compare(surnames[j], min_surname) == Ordering.Greater
                        ) {
                            continue;
                        }
                    }

                    min_name = names[j];
                    min_surname = surnames[j];
                    index = j;
                }
            }

            var temp = names[i];
            names[i] = names[index];
            names[index] = temp;

            temp = surnames[i];
            surnames[i] = surnames[index];
            surnames[index] = temp;
        }
    }

    public static void bubbleSort(String[] names, String[] surnames) {
        boolean wasSwapped;

        do {
            wasSwapped = false;

            for (int i = 0; i < names.length - 1; i++) {
                if (compare(names[i], names[i + 1]) == Ordering.Greater
                    || compare(names[i], names[i + 1]) == Ordering.Equal
                    && compare(surnames[i], surnames[i + 1]) == Ordering.Greater
                ) {
                    var temp = names[i];
                    names[i] = names[i + 1];
                    names[i + 1] = temp;

                    temp = surnames[i];
                    surnames[i] = surnames[i + 1];
                    surnames[i + 1] = temp;

                    wasSwapped = true;
                }
            }
        } while (wasSwapped);
    }

    public static void insertionSort(String[] names, String[] surnames) {
        for (int i = 0; i < names.length - 1; i++) {
            for (int j = i; j >= 0; j--) {
                if (compare(names[j + 1], names[j]) == Ordering.Greater
                    || compare(names[j + 1], names[j]) == Ordering.Equal
                    && compare(surnames[j + 1], surnames[j]) == Ordering.Greater
                ) {
                    break;
                }

                var temp = names[j + 1];
                names[j + 1] = names[j];
                names[j] = temp;

                temp = surnames[j + 1];
                surnames[j + 1] = surnames[j];
                surnames[j] = temp;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(compare("Aboba", "Abobav2") == Ordering.Less);
        System.out.println(compare("AmongSus", "AmongSus") == Ordering.Equal);
        System.out.println(compare("Acbd", "Abcd") == Ordering.Greater);

        final String[] originalNames = {
                "Egor",
                "Artiom",
                "Egor",
                "Vlad",
                "Igor",
                "Ivan",
                "Kirill",
        };

        final String[] originalSurnames = {
                "Anufriev",
                "Abikenov",
                "Kalinchuk",
                "Ilyin",
                "Loginov",
                "Minin",
                "Fomichev",
        };

        System.out.println("Original names:\t\t" + Arrays.toString(originalNames));
        System.out.println("Original surnames:\t" + Arrays.toString(originalSurnames));

        final String[] rightNames = {
                "Artiom",
                "Egor",
                "Egor",
                "Igor",
                "Ivan",
                "Kirill",
                "Vlad",
        };

        final String[] rightSurnames = {
                "Abikenov",
                "Anufriev",
                "Kalinchuk",
                "Loginov",
                "Minin",
                "Fomichev",
                "Ilyin",
        };

        System.out.println("Right names:\t\t" + Arrays.toString(rightNames));
        System.out.println("Right surnames:\t\t" + Arrays.toString(rightSurnames));
        System.out.println();

        var names = originalNames.clone();
        var surnames = originalSurnames.clone();
        directSelectionSort(names, surnames);
        System.out.println(Arrays.toString(names) + '\n' + Arrays.toString(surnames));
        System.out.println(Arrays.equals(names, rightNames) && Arrays.equals(surnames, rightSurnames));

        names = originalNames.clone();
        surnames = originalSurnames.clone();
        bubbleSort(names, surnames);
        System.out.println(Arrays.toString(names) + '\n' + Arrays.toString(surnames));
        System.out.println(Arrays.equals(names, rightNames) && Arrays.equals(surnames, rightSurnames));

        names = originalNames.clone();
        surnames = originalSurnames.clone();
        insertionSort(names, surnames);
        System.out.println(Arrays.toString(names) + '\n' + Arrays.toString(surnames));
        System.out.println(Arrays.equals(names, rightNames) && Arrays.equals(surnames, rightSurnames));

        // Testing with empty arrays
        names = new String[] {};
        surnames = new String[] {};

        directSelectionSort(names, surnames);
        bubbleSort(names, surnames);
        insertionSort(names, surnames);
    }
}

